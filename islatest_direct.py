#!/usr/bin/python3

from selenium import webdriver                                                                                         
import time                                                                                                            
import threading

def geturls():
  with open('urls.csv') as f:
    return [line.rstrip() for line in f]

def getusers():
  with open('users.csv') as f:
    return [line.rstrip() for line in f]

def background(f):
    def bg_f(*a, **kw):
      threading.Thread(target=f, args=a, kwargs=kw).start()
    return bg_f

@background
def tabnavigate(driver, urls):
  while True:
    for tab in range(0, len(urls)):
      driver.switch_to.window(driver.window_handles[tab])
      driver.implicitly_wait(3)

@background
def scroll(driver, speed):
    i = 0
    while True:
      if i == 21: i = -i
      if i > 0:
        driver.execute_script("window.scrollBy(0,%s)" %(speed),"")
        time.sleep(0.1) 
      else:
        revspeed = (-1)*speed
        driver.execute_script("window.scrollBy(0,%s)" %(revspeed),"")
        time.sleep(0.1)
      i = i + 1
    

def islalogin(driver, user):
  time.sleep(3)
  driver.find_element_by_id("login_username").send_keys(user)
  driver.find_element_by_id("login_pwd").send_keys("Admin@123")
  driver.find_element_by_id("login_submit").click()
  
def spinchrome(users, urls):
  for user in users:
      driver = webdriver.Chrome()
      driver.maximize_window()
      driver.get(urls[0])
      islalogin(driver, user)
      for url in urls[1:]:
        driver.execute_script("window.open(target='%s');" %(url)) 
      tabnavigate(driver, urls)
      speed = 100
      scroll(driver, speed)

if __name__ == "__main__":
  spinchrome(getusers(), geturls())
  time.sleep(100)



