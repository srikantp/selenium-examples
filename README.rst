README
======

A script based on selenium automation tool. Initially written to test various
browser based components of Isla. 

Requirements
------------

* A modern Linux based OS with Python3 and latest selenium from pip.
* Sufficient RAM to open multiple browsers in headless mode.
* A fast CPU, preferably a Desktop with i3 7th Gen or above.


