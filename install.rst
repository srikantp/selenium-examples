Install steps on debian 9 
-------------------------

sudo apt install -y curl unzip python3
curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
sudo python3 get-pip.py
sudo pip3 install selenium
LATEST=$(wget -q -O - http://chromedriver.storage.googleapis.com/LATEST_RELEASE)
wget http://chromedriver.storage.googleapis.com/$LATEST/chromedriver_linux64.zip
unzip chromedriver_linux64.zip && sudo mv $PWD/chromedriver /usr/local/bin/chromedriver
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt install -f

python3 islatest.py config.ini

# Do a soft link as well if you want it to run from cronjob
# ln -s /usr/local/bin/chromedriver /bin/chromedriver
# Set cronjob as regular user
# crontab -u spike -e
## @reboot Xvfb :99
## @reboot /usr/bin/python3 /opt/islatest.py &> /tmp/error


