from selenium import webdriver
import time
import os
import sys
import threading
#testURL = "https://cc.cyberinc.com/client.html#https://www.cnn.com"
testURL = "https://www.cnn.com"

# Extracting and ordering data from config.ini
def readConfig(configFile):
    global browserConfigs, testusersConfigs, seleniumConfigs, testurlsConfigs, reportConfigs
    try:
        with open(configFile) as f:
            allConfigs = [line for line in f.read().splitlines() if not line.startswith('#') and len(line) >  0]
        browserConfigs   = [line for line in allConfigs if line.startswith('browser.')]
        testusersConfigs = [line for line in allConfigs if line.startswith('testusers.')]
        seleniumConfigs  = [line for line in allConfigs if line.startswith('selenium.')]
        testurlsConfigs  = [line for line in allConfigs if line.startswith('testurls.')]
        reportConfigs    = [line for line in allConfigs if line.startswith('report.')]
        print(extract(browserConfigs, 1))
    except Exception as e:
        print(e)

def extract(configList, index):
    return configList[index].split('=')[-1].strip()

def background(f):
    "a threading decorator use @background above the function you want to run in the background"
    def bg_f(*a, **kw):
        threading.Thread(target=f, args=a, kwargs=kw).start()
    return bg_f

def browserInit(testURL, uname, passwd):
    os.system("rm -rf /tmp/%s" %uname)
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--%s" %(extract(browserConfigs, 1)))
    chrome_options.add_argument("--headless")
    #chrome_options.add_argument("--user-data-dir=/tmp/%s" %uname)
    driver = webdriver.Chrome(chrome_options=chrome_options)
    #driver.implicitly_wait(30)
    driver.get(testURL)
    assert "Isolating" in driver.title
    time.sleep(1)
    loginIsla(driver, uname, passwd)

@background
def loginIsla(driver, uname, passwd):
    username = driver.find_element_by_class_name("signinUsernameText")
    password = driver.find_element_by_class_name("signinPasswordText")
    username.send_keys(uname)
    password.send_keys(passwd)
    driver.find_element_by_class_name("signinFormButton").click()
    print(uname)
    time.sleep(300000)
    driver.close()


if __name__ == "__main__":
    if (len(sys.argv) is 2) and (os.path.exists(sys.argv[1]) and (sys.argv[1].endswith('.ini'))):
        readConfig(sys.argv[1])
     #   print(browserConfigs, testusersConfigs, seleniumConfigs, testurlsConfigs, reportConfigs)
        for u in range(1,2):
            browserInit(testURL, "user{}".format(u), "user@123")
            time.sleep(2)
        #driver.close()
    else:
        print('Usage:', 'python islatest.py <path of config.ini>')
